import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'http4';
  todo: any;

  ngOnInit() {}

  constructor(private http: HttpClient) {}

  onCreatePost(postData: { title: string; content: string }) {
    // Send Http request
    this.http
      .post(
         'https://sample-9b460-default-rtdb.firebaseio.com/data.json',
        // 'http://localhost:5555/products',
        postData
      )
      .subscribe((responseData) => {
        console.log(responseData);
      });
  }

  onShow() {
    this.http
      .get('https://sample-9b460-default-rtdb.firebaseio.com/data.json')
      // .get('http://localhost:5555/products')
      .subscribe((data: any) => {
        console.table(data);
        this.todo = data;
      });
  }  
  onDelete(){this.http.
    delete('https://sample-9b460-default-rtdb.firebaseio.com/data.json')
    .subscribe((ResponseData)=>{
      console.log(ResponseData);
    });
   }
}
